# Jenkins X Installation

## Working directory
```
mkdir jx-gcp
cd jx-gcp
```
## GCP Setup and Terraform
```
export GOOGLE_APPLICATION_CREDENTIALS=~/Downloads/container-254002-40f2d3b5080e.json 
cat << EOF > main.tf
module "jx" {
  source  = "jenkins-x/jx/google"

  gcp_project = "container-254002"
}
EOF
terraform apply
```
## Install jx
`curl -L "https://github.com/jenkins-x/jx/releases/download/$(curl --silent "https://github.com/jenkins-x/jx/releases/latest" | sed 's#.*tag/\(.*\)\".*#\1#')/jx-linux-amd64.tar.gz" | tar xzv "jx"`
## Install helm
`curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash`
## Bootstrap
```
jx create cluster gke --skip-installation
git clone https://github.com/jenkins-x/jenkins-x-boot-config
cd jenkins-x-boot-config/
mv ../jx-requirements.yml .
jx boot -r jx-requirements.yml 
```
## Quickstart
```
jx create quickstart
jx get activity -f nodex -w
jx get activity nodex
```
## UI
```
jx add app jx-app-ui
jx ui
```
## Nexus
`kubectl port-forward service/nexus 8080:80`
